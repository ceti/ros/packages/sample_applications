#include "PickPlacePlanner.h"

#include <gtest/gtest.h>

class Grasping : public ::testing::Test {
public:
    Grasping(): spinner(nullptr) {};

protected:

    ros::AsyncSpinner* spinner;
    ros::NodeHandle* node;

    void SetUp() override {
        ::testing::Test::SetUp();

        this->node = new ros::NodeHandle("~");
        this->spinner = new ros::AsyncSpinner(2);
        this->spinner->start();

        ROS_INFO(">>>>>>>>>>>>>>>> WAITING FOR ROBOT INIT <<<<<<<<<<<<<<<");
        ros::Duration(3.0).sleep();
        ROS_INFO(">>>>>>>>>>>>>>>>> WAKING UP AFTER INIT <<<<<<<<<<<<<<<<");

        // clean up scene
        CleanupScene();
    }


    static void CleanupScene() {
        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

        for (auto p : planning_scene_interface.getObjects()) {
            p.second.operation = moveit_msgs::CollisionObject::REMOVE;
            planning_scene_interface.applyCollisionObject(p.second);
        }
        ASSERT_EQ(planning_scene_interface.getObjects().size(), 0);
        ASSERT_EQ(planning_scene_interface.getAttachedObjects().size(), 0);
    }

    void TearDown() override
    {
        ros::shutdown();
        delete this->spinner;
        delete this->node;
        ::testing::Test::TearDown();
    }

};


TEST_F(Grasping, PickPlacePlannerTest) {
    ros::WallDuration(1.0).sleep();
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    moveit::planning_interface::MoveGroupInterface group("panda_arm");
    group.setPlanningTime(45.0);

    pick_place_planner::addCollisionObjects(planning_scene_interface);

    // Wait a bit for ROS things to initialize
    ros::WallDuration(1.0).sleep();

    ASSERT_EQ(pick_place_planner::pick(group), moveit::planning_interface::MoveItErrorCode::SUCCESS);

    ros::WallDuration(1.0).sleep();

    ASSERT_EQ(pick_place_planner::place(group), moveit::planning_interface::MoveItErrorCode::SUCCESS);

}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "panda_arm_pick_place_test");
    ::testing::InitGoogleTest(&argc, argv);

    int result = RUN_ALL_TESTS();

    return result;
}


