cmake_minimum_required(VERSION 2.8.3)
project(sample_applications)

add_compile_options(-std=c++14)
set(GCC_COVERAGE_COMPILE_FLAGS "-fpermissive")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS} ${GAZEBO_CXX_FLAGS}")

# Find catkin macros and libraries if COMPONENTS list like `find_package(catkin REQUIRED COMPONENTS xyz)` is used,
# also find other catkin packages
find_package(catkin REQUIRED
        COMPONENTS controller_manager
        effort_controllers
        gazebo_ros
        genmsg
        joint_state_controller
        joint_state_publisher
        joint_trajectory_controller
        robot_state_publisher
        roscpp
        tf2
        tf2_geometry_msgs
        std_msgs
        xacro
        moveit_core
        moveit_visual_tools
        moveit_ros_planning
        moveit_ros_planning_interface
        controller_interface
        hardware_interface
        simulation_util)

# System dependencies are found with CMake's conventions
find_package(PkgConfig REQUIRED)

# ######################################################################################################################
# catkin specific configuration ##
# ######################################################################################################################
# The catkin_package macro generates cmake config files for your package and declare things to be passed to dependent
# projects. This has to be done in addition to the find_package command above.
catkin_package(CATKIN_DEPENDS
        moveit_core
        moveit_visual_tools
        moveit_ros_planning_interface
        controller_interface
        hardware_interface
        pluginlib
        tf2
        simulation_util
        DEPENDS
        # system_lib
        )

# ######################################################################################################################
# Build ##
# ######################################################################################################################

# Specify additional locations of header files Your package locations should be listed before other locations
include_directories(src ${catkin_INCLUDE_DIRS})

add_executable(BasicCartesianPlanner src/BasicCartesianPlanner.cpp)
add_executable(BasicJointSpacePlanner src/BasicJointSpacePlanner.cpp)
add_executable(ObstacleAwarePlanner src/ObstacleAwarePlanner.cpp)
add_executable(PickPlacePlanner src/PickPlacePlanner.cpp src/PickPlacePlannerMain.cpp)

## Add cmake target dependencies of the executable
target_link_libraries(BasicCartesianPlanner ${catkin_LIBRARIES})
target_link_libraries(BasicJointSpacePlanner ${catkin_LIBRARIES})
target_link_libraries(ObstacleAwarePlanner ${catkin_LIBRARIES})
target_link_libraries(PickPlacePlanner ${catkin_LIBRARIES})

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries

if(CATKIN_ENABLE_TESTING)
    find_package(rostest REQUIRED)
    add_rostest_gtest(tests_grasping test/grasping.test test/test.cpp src/PickPlacePlanner.cpp)
    target_link_libraries(tests_grasping ${catkin_LIBRARIES})
endif()